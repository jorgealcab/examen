﻿USE examen;

-- 1 Código y nombre de los productos que valen más de 45€
  SELECT p.pid, p.descripcion FROM productos p WHERE p.precio>45;

-- 2 Código de las tiendas donde hay unidades del producto P2
  SELECT t.tid FROM texistencias t WHERE t.pid='P2';

-- 3 Código y fecha de pedido de los pedidos de mas de 11 unidades que hayan hecho 
-- los almacenes A2 y A5
  SELECT c1.pedid,c1.fped FROM (
  SELECT * FROM pedidos p WHERE p.aid='A2' OR 'A5') c1
  WHERE c1.cant > 11;

-- 4 Listado de nombres de los productos y su precio, añadiéndole una columna con el precio
-- con IVA 21%
  SELECT p.descripcion, p.precio,p.precio*1.21 pvp FROM productos p;

-- 5 Cantidad total y media de productos por tienda
  SELECT t.tid,SUM(t.tcant),AVG(t.tcant) FROM texistencias t GROUP BY t.tid;

-- 6 Contar el número de tiendas
  SELECT COUNT(t.tid) FROM tiendas t;

-- 7 Nombres de las ciudades donde no haya tienda ni almacén
  DROP TEMPORARY TABLE IF EXISTS temp1;
  CREATE TEMPORARY TABLE temp1 (loc char(15),tloc char(15),aloc char(15));
  INSERT INTO temp1(loc) 
    VALUES ('Barcelona'),('Bilbao'),('Madrid'),('Sevilla'),('Huelva'),('Murcia'),('Granada');
  

  SELECT t.loc loc FROM temp1 t LEFT JOIN(
    SELECT DISTINCT tloc loc FROM tiendas union
    SELECT DISTINCT aloc loc FROM almacenes)c1 ON t.loc=c1.loc
  WHERE c1.loc IS null;

  DROP TEMPORARY TABLE temp1;



-- 8 Indicarme la descripción del producto que está en todas las tiendas
  SELECT p.descripcion FROM productos p JOIN (
    SELECT count(t.tid) cuenta, t.pid FROM texistencias t GROUP BY t.pid
     HAVING cuenta=(SELECT COUNT(*) FROM tiendas)) c1 ON p.pid=c1.pid;

-- 9 Indicarme la descripción del producto que no esta en ninguno de los almacenes
 SELECT p.descripcion FROM productos p JOIN (
   SELECT COUNT(a.aid) cuenta, a.pid FROM aexistencias a GROUP BY a.pid
    HAVING cuenta=0) c1 ON p.pid=c1.pid;


-- EJERCICIO 5 Crear la siguiente tabla
DROP TABLE IF EXISTS personas;
CREATE TABLE personas(
	dni int,
  cod_hospital int,
	apellidos varchar(50),
  funcion varchar(20),
  salario int,
  PRIMARY KEY (dni)
  )
ENGINE = InnoDB
CHARACTER SET utf8 COLLATE utf8_spanish_ci;

INSERT HIGH_PRIORITY INTO personas( dni, cod_hospital, apellidos, funcion, salario)
  VALUES
    (12345678,1,'García Hernández, Eladio','CONSERJE',1200),
    (22233311,4,'Martínez Molina, Gloria','MÉDICO',1600),
    (22233322,2,'Tristán García, Ana','MÉDICO',1900),
    (22233333,2,'Martínez Molina, Andrés','MÉDICO',1600),
    (33222111,4,'Mesa del Castillo, Juan','MÉDICO',2200),
    (55544411,3,'Ruiz Hernández, Caridad','MÉDICO',1900),
    (55544412,4,'Jiménez Jiménez, Dolores','CONSERJE',1200),
    (55544433,2,'González Marín, Alicia','CONSERJE',1200),
    (66655544,1,'Castillo Montes, Pedro','MÉDICO',1700),
    (87654321,1,'Fuentes Bermejo, Carlos','DIRECTOR',2000),
    (99988333,3,'Serrano Díaz, Alejandro','DIRECTOR',2400);
   

-- EJERCICIO 6 
-- 1 Dada la tabla personas insertar a una persona de apellidos y nombre 'Quiroge Rojas, Leopoldo',
-- cuya función sea 'CONSERJE', con DNI '456788999' y con el código de hospital 4.
INSERT INTO personas (dni, cod_hospital, apellidos, funcion)
  VALUES (456788999, 4, 'Quiroge Rojas, Leopoldo', 'CONSERJE');

-- 2 Inserta en la tabla personas una persona de nombre 'Serrano Ruiz, Antonio', con DNI '111333222'
-- perteneciente al hospital número 3.
INSERT INTO personas (dni, cod_hospital, apellidos, funcion)
  VALUES (111333222, 3, 'Serrano Ruiz, Antonio', 'CONSERJE');

-- 3 Crear una tabla igual que personas pero sin datos. Llamarla personas1 (utilizando consultas)
CREATE TABLE personas1 AS SELECT * FROM personas;

-- 4 Inserta en la tabla personas1 los datos de las personas que trabajan en el hospital
-- numero 1 (INSERT con SELECT)
INSERT INTO personas1 ( dni, cod_hospital, apellidos, funcion, salario)
 SELECT * FROM personas WHERE cod_hospital=1;
 
-- 5 Crear una tabla llamada personas2. con los campos (DNI,APELLIDOS,FUNCION) de personas
-- utilizando consultas
CREATE TABLE personas2 AS SELECT dni,apellidos,funcion
 FROM personas;

-- 6 Se ha creado una nueva tabla llamada personas2. Esta tabla tiene los siguientes campos
-- (DNI,APELLIDOS,FUNCION). ¿Cómo podremos introducir en esa tabla los datos de las personas
-- del código de hospital 4?
INSERT INTO personas2 (dni,apellidos,funcion)
 SELECT dni,apellidos,funcion FROM personas WHERE cod_hospital=4;

-- 7 Inserta en la tabla personas una persona con dni 99887766 y apellidos 'Martínez Martínez,
-- Alejandro' en el hospital que tiene tan solo 1 persona (insert con select).
INSERT INTO personas (dni,apellidos,cod_hospital)
 VALUES( 
    '99887766',
    'Martínez Martínez, Alejandro',
     (SELECT DISTINCT p.cod_hospital FROM personas p JOIN(
      SELECT COUNT(*) cuenta,cod_hospital FROM personas 
        GROUP BY cod_hospital HAVING cuenta=1) c1
        on p.cod_hospital=c1.cod_hospital)
      );
 